class EventEmitter {
  emit(name, data) {
    console.log(name, data);
  }
}

class Element extends EventEmitter {
  constructor(logic, data) {
    super();
    this.logic = logic;
    this.data = data;

    this.isLoaded = false;
  }

  load() {
    console.log("load")
  }

  play() {
    console.log("play");
  }

  _onLoad() {
    this.emit('loaded', {});
    this.isLoaded = true;
    return this;
  }
}

class Image extends Element {
  load() {
    return this.logic.getMediaUrl(this.data.media)
      .then(url => /*load Image*/)
      .then(img => this._draw(img))
      .then(() => super._onLoad())
  }

  play() {
    console.log("animate");
  }

  _draw(url) {
    return new Promise((resolve, reject) => {
      //draw Image
    });
  }
}

const fabricElement = {
  "image": Image,
  "video": Element,
  "container": Container
};


class Container extends Element {
  load() {
    this.promises = this.data.elements.map(element => {
      const component = new fabricElement[element.type](this.logic, element.data);
      return component.load();
    });

    return Promise.all(this.promises)
      .then((components) => {
        this.components = components;
        return this._onLoaded();
      });
  }

  play() {
    this.components.forEach((c) => c.play());
  }
}


